from django.http import HttpResponse


# Create your views here.

def home(request):
    return HttpResponse("localhost/home")


def home_lesson_2(request):
    return HttpResponse("localhost/lesson_2/")


def book(request, title):
    return HttpResponse(f'<title>{title}</title>')


def regex(request, txt):
    return HttpResponse(f"localhost/lesson_2/regex.n/{txt}")


def bio(request, user_name):
    return HttpResponse(f'USER NAME > {user_name}')
