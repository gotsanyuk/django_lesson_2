from django.urls import path, re_path

from lesson_2 import views

urlpatterns = [
    path('', views.home_lesson_2, name='lesson_2'),  # http://localhost/lesson_2/
    re_path(r'regex1/([db]o[gx])/', views.regex, name='dog'),  # http://localhost/lesson_2/regex1/dog/
    re_path(r'regex2/([^db]o[^gx])/', views.regex, name='cot'),  # http://localhost/lesson_2/regex2/cot/
    re_path(r'regex3/([dog]{0,5})/', views.regex, name='dogog'),  # http://localhost/lesson_2/regex3/dogog/
]
