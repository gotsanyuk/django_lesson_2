from django.urls import path, re_path

from lesson_1 import views

urlpatterns = [
    path('', views.home_lesson_1, name='lesson_1'),  # http://localhost/lesson_1/
    re_path(r'regex1/(c[abt]t)/', views.regex, name='cat'),  # http://localhost/lesson_1/regex1/cat/
    re_path(r'regex2/(c[^abt]t)/', views.regex, name='no_cat'),  # http://localhost/lesson_1/regex2/cit/
    re_path(r'regex3/([cat]{0,11})/', views.regex, name='catat'),  # http://localhost/lesson_1/regex3/catatatatat/
    re_path(r'regex4/([cat]{0,3})/', views.regex, name='catcat')  # http://localhost/lesson_1/regex4/
]
